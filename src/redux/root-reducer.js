import { combineReducers } from 'redux';
import { types } from './actions';

/**
 * Root reducers containing combine reducer state from Overview and Details
*/

const city = (state='', action) => {
    switch (action.type) {
        case types.CHANGE_CITY:
            return action.city;
        default:
            return state;
    }
}

const initialState = {
    isFetching: false,
    weathers: null,
    error: null,
}

const weatherData = (state = initialState, action) => {
    switch (action.type) {
        case types.REQUEST_WEATHER:
            return Object.assign({}, state, {
                isFetching: true,
              });
        case types.RECEIVE_WEATHER_ERROR:
            console.log(action.error)
            return Object.assign({}, state, {
                isFetching: false,
                weathers: null,
                error: action.error,
            })
        case types.RECEIVE_WEATHER:
            const weathers = {
                temp: [],
                pressure: [],
                humidity: [],
            }
            if (!action.weathers.list) {
                return Object.assign({}, state, {
                    isFetching: false,
                    error: action.weathers.message,
                    weathers: null,
                  });
            }
            action.weathers.list.forEach((w, i) => {
                weathers.temp.push({x: i, y: w.main.temp});
                weathers.pressure.push({x: i, y: w.main.pressure});
                weathers.humidity.push({x: i, y: w.main.humidity});
            })
            return Object.assign({}, state, {
                isFetching: false,
                weathers,
                error: null
              });
        default:
            return state;
    }
}

export default combineReducers({
    city,
    weatherData,
});