import { API_PATH } from '../constants';

const types = {
    CHANGE_CITY: 'CHANGE_CITY',
    REQUEST_WEATHER: 'REQUEST_WEATHER',
    RECEIVE_WEATHER: 'RECEIVE_WEATHER',
    RECEIVE_WEATHER_ERROR: 'RECEIVE_WEATHER_ERROR'
}

const changeCity = (city) => {
    return {
      type: types.CHANGE_CITY,
      city
    }
}

const requestWeather = () => {
    return {
      type: types.REQUEST_WEATHER,
    }
}

const receiveWeather = (list) => {
    return {
        type: types.RECEIVE_WEATHER,
        weathers: list,
    }
}   

const receiveWeatherFailed = (err) => {
    return {
        type: types.RECEIVE_WEATHER_ERROR,
        error: err,
    }
} 

const fetchWeather = (city) => {
    return (dispatch) => {
        dispatch(changeCity(city))
        dispatch(requestWeather());
        const api = API_PATH(city);
        return fetch(api)
        .then(res => res.json())
        .then(res => dispatch(receiveWeather(res)))
    }
}

export {
    types,
    fetchWeather,
}