import React from 'react';
import { connect } from "react-redux";
import {Link} from 'react-router-dom';
import { Loader } from '../../components';
import { VictoryChart, VictoryTheme, VictoryLine, VictoryAxis } from 'victory';

const mapStateToProps = (state) => ({
    isFetching: state.weatherData.isFetching,
    weatherData: state.weatherData.weathers,
    error: state.weatherData.error,
});

const chartStyle = {
    data : { stroke: "#c43a31" },
    parent: { border: "1px solid #ccc"}
}

class Report extends React.Component {
    getChartData = (data, error) => {
        if (!data) {
            return <h3>{error || 'No data available to display. Please go back and enter another city'}</h3>
        }
        const { temp, pressure, humidity } = data;
        return (
            <div className='chart-group'>
                <div className='chart-container'>
                    <VictoryChart theme={VictoryTheme.material} animate={{ duration: 1000 }}>
                        <VictoryLine
                            style={chartStyle}
                            data={temp}
                        />
                        <VictoryAxis dependentAxis />
                        <VictoryAxis tickValues={[8, 16, 24, 32, 40]} tickFormat={t => `Day-${t/8}`} />
                    </VictoryChart>
                    <b>Temperature</b>
                </div>
                <div className='chart-container'>
                    <VictoryChart theme={VictoryTheme.material} animate={{ duration: 1000 }}>
                        <VictoryLine
                            style={chartStyle}
                            data={pressure}
                        />
                        <VictoryAxis dependentAxis />
                        <VictoryAxis tickValues={[8, 16, 24, 32, 40]} tickFormat={t => `Day-${t/8}`} />
                    </VictoryChart>
                    <b>Pressure</b>
                </div>
                <div className='chart-container'>
                    <VictoryChart theme={VictoryTheme.material} animate={{ duration: 1000 }}>
                        <VictoryLine
                            style={chartStyle}
                            data={humidity}
                        />
                        <VictoryAxis dependentAxis />
                        <VictoryAxis tickValues={[8, 16, 24, 32, 40]} tickFormat={t => `Day-${t/8}`} />
                    </VictoryChart>
                    <b>Humidity</b>
                </div>
            </div>
        );
    }
    render() {
        const { isFetching, weatherData } = this.props;
        if (isFetching) {
            return <Loader />
        }
        return (
            <React.Fragment>
                <Link to="/" className='btn btn-small' onClick={this.handleBack}>Back</Link>
                <h2 className='forecast-head'>Forecasts - Every 3 hour for next 5 days</h2>
                {this.getChartData(weatherData)}
            </React.Fragment>
        );
    }
}

export default connect(
    mapStateToProps,
    null,
)(Report);