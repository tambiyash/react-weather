import React from 'react';
import { connect } from "react-redux";
import { fetchWeather } from '../../redux/actions';
import { Link } from 'react-router-dom';

const mapDispatchToProps = (dispatch) => ({
    fetchWeatherByCity: (city) => dispatch(fetchWeather(city)),
});

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
        }
    }
    handleChange = (evt) => {
        this.setState({
            input: evt.target.value,
        })
    }
    handleClick = () => {
        this.props.fetchWeatherByCity(this.state.input);
    }
    render() {
        return (
            <div className='Form-Container'>
                <h1>Weather Forecast</h1>
                <h3 className='forecast-head'> Just enter name of any city and know about it's weather conditions like Temperature, Pressure and Humidity for the next 5 days !</h3>
                <input className='input-box' type='text' placeholder='Enter a city name' value={this.state.input} onChange={this.handleChange} />
                <Link className='btn' to='/report' onClick={this.handleClick}>Get the Forecast</Link>
            </div>
        );
    }
}

export default connect(
    null,
    mapDispatchToProps,
)(Home);