import React from 'react';

const Loader = () => (
    <div className="loader-1">
      <div className="loader-outer"></div>
      <div className="loader-inner"></div>
    </div>
)

export default Loader;