import NotFound from './NotFound';
import Loader from './Loader';

export {
    NotFound,
    Loader
};