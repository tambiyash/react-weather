import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Home, Report } from './containers';
import { NotFound } from './components';

const App = () => (
  <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
    </header>
    <div className='Main-Container'>
      <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/report/" component={Report} />
                <Route component={NotFound} />
            </Switch>          
        </BrowserRouter>
    </div>
  </div>
)

export default App;
