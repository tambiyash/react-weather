const API_KEY = 'dffb5399a7a11906bb7a0baa3bc71be9';
const API_PATH = (city_name, key=API_KEY) => `http://api.openweathermap.org/data/2.5/forecast?q=${city_name}&appid=${key}`;

export {
    API_PATH,
}